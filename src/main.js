// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import {
  router
} from './router'
import VueBlu from 'vue-blu'
import Vuelidate from 'vuelidate'
import store from './config/store'
import VueScrollTo from 'vue-scrollto'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueYouTubeEmbed from 'vue-youtube-embed'
import VTooltip from 'v-tooltip'
import VueCarousel from 'vue-carousel'
import VueCookie from 'vue-cookie'

import 'vue-blu/dist/css/vue-blu.min.css'
import './assets/css/global.css'

Vue.use(VueCarousel)
Vue.use(VTooltip)
Vue.use(VueCookie);
Vue.use(VueYouTubeEmbed)
Vue.use(Vuelidate)
Vue.use(VueBlu)
Vue.use(VueScrollTo)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCBE8n8ob-amB9zM_Uo1uZ13TxclYFBCVs',
    libraries: 'places'
  }
})

Vue.config.productionTip = false

new Vue({
  router,
  store
}).$mount('#app')
