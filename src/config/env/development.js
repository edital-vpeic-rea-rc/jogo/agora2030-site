module.exports = {
  'process.env': {
    NODE_ENV: '"development"',
    DEBUG_MODE: true,
    SERVER: '"http://localhost:3000/v1/"',
    STATIC_URL: '"http://localhost:3000/static"',
    HOST: '"http://localhost:8080"',
    BLOG: '"https://blog.colabtech.org/index.php/"'
  }
}
