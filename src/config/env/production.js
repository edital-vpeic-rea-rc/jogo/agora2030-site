module.exports = {
  'process.env': {
    NODE_ENV: '"production"',
    DEBUG_MODE: false,
    SERVER: `"${process.env.API_URL}"`,
    STATIC_URL: `"${process.env.STATIC_URL}"`,
    HOST: `"${process.env.HOST}"`,
    BLOG: `"${process.env.BLOG}"`
  }
}
