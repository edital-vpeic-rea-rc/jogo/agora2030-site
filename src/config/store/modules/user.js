import { getUserProfile } from '../../../api/user.service'

const state = {
  logged: {},
  userRoles: ['admin', 'moderator', 'coordenador', 'superadmin', 'eventStaff', 'user'],
  rolesAdmin: ['admin', 'moderator', 'coordenador', 'superadmin', 'eventStaff'],
  permissions: {
    admin: {
      CHANGE_TYPE: true,
      CHANGE_ROLE: true,
      MENU_SHOW_SOLUTION: true,
      MENU_SHOW_QUESTION: true,
      MENU_SHOW_EVENT: true,
      EDIT_SOLUTION: true,
      EDIT_EVENT: true,
      EVENTS_CREATE: true,
      EVENT_SUBSCRIPTION_PAGE: true,
      SHOW_SOLUTION_FILTER: true,
      CHANGE_PUB_STATUS: true,
      SOLUTION_ADD_MOD: true,
      WORKSHOP_ADD: true,
      WORKSHOP_EDIT: true,
      WORKSHOP_HELPER: true,
      WORKSHOP_MEMBERS_LINK: true,
      MANAGE_USERS: true,
      GENERATE_REPORTS: true,
      USER_CONFIRM_EMAIL: true
    },
    coordenador: {
      CHANGE_PUB_STATUS: true,
      CHANGE_ROLE: true,
      MENU_SHOW_QUESTION: true,
      MENU_SHOW_SOLUTION: true,
      MENU_SHOW_EVENT: true,
      EDIT_SOLUTION: true,
      EDIT_EVENT: true,
      EVENT_SUBSCRIPTION_PAGE: true,
      SHOW_SOLUTION_FILTER: true,
      SOLUTION_ADD_MOD: true,
      WORKSHOP_ADD: true,
      WORKSHOP_EDIT: true,
      WORKSHOP_HELPER: true,
      WORKSHOP_MEMBERS_LINK: true,
      MANAGE_USERS: true,
      GENERATE_REPORTS: true,
      USER_CONFIRM_EMAIL: true
    },
    moderator: {
      MENU_SHOW_SOLUTION: true,
      MENU_SHOW_EVENT: true,
      EDIT_SOLUTION: true,
      SHOW_SOLUTION_FILTER: true,
      CHANGE_PUB_STATUS: true,
      WORKSHOP_MEMBERS_LINK: true,
      MANAGE_USERS: true,
      USER_CONFIRM_EMAIL: true
    },
    eventStaff: {
      MENU_SHOW_EVENT: true,
      WORKSHOP_MEMBERS_LINK: true,
      MANAGE_USERS: true
    },
    user: {}
  }
}

const getters = {
  me: (state) => state.logged,
  userRoles: (state) => state.userRoles,
  getPerm: (state) => (permission, role) => {
    if (!role) {
      if (!state.logged.role) {
        return false
      }
      role = state.logged.role
    }
    return state.permissions[role][permission]
  },
  canAccessAdmin: (state) => {
    if (state.logged.role === 'user') {
      return false
    } else {
      if (state.rolesAdmin.indexOf(state.logged.role) > -1) {
        return true
      }
    }
  }
}

const mutations = {
  saveMe (state, payload) {
    state.logged = payload
  },
  removeMe (state) {
    state.logged = {}
  }
}

const actions = {
  doSaveMe ({ state, commit }, userdata) {
    return new Promise((resolve, reject) => {
      if (userdata && !userdata.token) {
        commit('saveMe', userdata)
        resolve()
      } else {
        let token
        if (userdata && userdata.token) {
          token = userdata.token
        }
        getUserProfile(token)
          .then(
            success => {
              commit('saveMe', success.data)
              resolve()
            },
            error => {
              reject(error)
            }
          )
      }
    })
  },
  doRemoveMe ({ state, commit }) {
    commit('removeMe')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
