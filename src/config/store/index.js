import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import iniciativa from './modules/iniciativa'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    iniciativa
  }
})
