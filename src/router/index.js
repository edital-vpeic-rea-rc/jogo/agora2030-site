import Vue from 'vue'
import Router from 'vue-router'

const RootPage = () => import(/* webpackChunkName: "RootPage" */ '../components/RootPage.vue')
const AccessDenied = () => import(/* webpackChunkName: "AccessDenied" */ '../components/403.vue')

const AdicionarIniciativaPage = () => import(/* webpackChunkName: "AdicionarIniciativaPage" */ '../components/AdicionarIniciativa.vue')
const AdicionarIniciativaEtapa2Page = () => import(/* webpackChunkName: "AdicionarIniciativaEtapa2Page" */ '../components/AdicionarIniciativa-capa.vue')
const AdicionarIniciativaEtapa3Page = () => import(/* webpackChunkName: "AdicionarIniciativaEtapa3Page" */ '../components/AdicionarIniciativa-perguntas.vue')
const AdicionarIniciativaEtapa4Page = () => import(/* webpackChunkName: "AdicionarIniciativaEtapa4Page" */ '../components/AdicionarIniciativa-mapa.vue')
const LoginPage = () => import(/* webpackChunkName: "LoginPage" */ '../components/Login.vue')
const InitiativesPage = () => import(/* webpackChunkName: "InitiativesPage" */ '../components/Iniciativas.vue')
const QuestionsPage = () => import(/* webpackChunkName: "QuestionsPage" */ '../components/Questions.vue')
const QuestionAddPage = () => import(/* webpackChunkName: "QuestionAddPage" */ '../components/QuestionsAdd.vue')

const ConfirmEmailPage = () => import(/* webpackChunkName: "ConfirmEmailPage" */ '../components/confirm-email.vue')
const ChangePasswordPage = () => import(/* webpackChunkName: "ChangePasswordPage" */ '../components/change-password.vue')

const LandPage = () => import(/* webpackChunkName: "LandPage" */ '../components/land/landpage.vue')
const InitiativePage = () => import(/* webpackChunkName: "InitiativePage" */ '../components/land/initiativepage.vue')
const CuratorsPage = () => import(/* webpackChunkName: "CuratorsPage" */ '../components/land/curatorspage.vue')
const Reports = () => import(/* webpackChunkName: "LandPage" */ '../components/Reports.vue')
const Users = () => import(/* webpackChunkName: "LandPage" */ '../components/Users.vue')
const Comments = () => import(/* webpackChunkName: "LandPage" */ '../components/Comments.vue')

import store from '../config/store'

Vue.use(Router)

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: LandPage
    },
    {
      path: '/initiative/:id',
      component: InitiativePage,
      name: 'initiative'
    },
    {
      path: '/curators',
      component: CuratorsPage,
      name: 'curators'
    },    
    {
      path: '/accountconfirmation',
      component: ConfirmEmailPage
    },
    {
      path: '/recuperar-senha',
      component: ChangePasswordPage
    },
    {
      path: '/admin',
      component: RootPage,
      children: [
        {
          path: '/',
          alias: ['/admin/iniciativas'],
          name: 'Iniciativas',
          component: InitiativesPage
        },
        {
          path: '/admin/iniciativa/adicionar',
          name: 'Adicionar Iniciativa',
          component: AdicionarIniciativaPage
        },
        {
          path: '/admin/iniciativa/:id/editar',
          name: 'Editar Iniciativa',
          component: AdicionarIniciativaPage
        },
        {
          path: '/admin/iniciativa/:id/capa',
          name: 'Adicionar capa a iniciativa',
          component: AdicionarIniciativaEtapa2Page
        },
        {
          path: '/admin/iniciativa/:id/capa/:action',
          name: 'Editar capa da iniciativa',
          component: AdicionarIniciativaEtapa2Page
        },
        {
          path: '/admin/iniciativa/:id/perguntas',
          name: 'Adicionar tags e perguntas',
          component: AdicionarIniciativaEtapa3Page
        },
        {
          path: '/admin/iniciativa/:id/perguntas/:action',
          name: 'Editar tags e perguntas',
          component: AdicionarIniciativaEtapa3Page
        },
        {
          path: '/admin/iniciativa/:id/localizacao',
          name: 'Adicionar localização',
          component: AdicionarIniciativaEtapa4Page
        },
        {
          path: '/admin/iniciativa/:id/comentarios',
          name: 'Moderar comentários',
          component: Comments
        },
        {
          path: '/admin/pergunta/adicionar',
          name: 'Adicionar Pergunta',
          component: QuestionAddPage
        },
        {
          path: '/admin/pergunta/:id/editar',
          name: 'Editar Pergunta',
          component: QuestionAddPage
        },
        {
          path: '/admin/perguntas',
          name: 'Perguntas',
          component: QuestionsPage
        },
        {
          path: '/admin/relatorios',
          name: 'Reports',
          component: Reports
        },
        {
          path: '/admin/usuarios',
          name: 'Users',
          component: Users
        }
      ],
      beforeEnter: (to, from, next) => {
        if (!store.state.user.logged.email) {
          store.dispatch('doSaveMe').then(
            success => {
              next()
            },
            error => {
              console.log('beforeEnter error', error)
              next({ path: '/admin/login' })
            }
          )
        }
      }
    },
    {
      path: '/403',
      component: AccessDenied
    },
    {
      path: '/admin/login',
      component: LoginPage
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
        // , offset: { x: 0, y: 10 }
      }
    }
    if (savedPosition) {
      return savedPosition
    } else {
      return {
        x: 0,
        y: 0
      }
    }
  }
})
