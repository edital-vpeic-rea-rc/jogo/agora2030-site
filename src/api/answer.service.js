import axios from 'axios'
import { getCookie } from './helpers'

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

const doRedirect = error => {
  switch (error) {
    case 403:
      window.location.href = '/403'
      break
    case 401:
      window.location.href = '/login'
      break
    default:
  }
}

const removeAnswer = (answerId, solutionId) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'delete',
      url: `${process.env.SERVER}initiatives/${solutionId}/answers/${answerId}`,
      headers: HEADERS,
      crossDomain: true,
      dataType: 'text'
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)

      doRedirect(error.status)
      reject(error)
    })
  })
}

const editAnswer = (answerId, solutionId, data) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'patch',
      url: `${process.env.SERVER}initiatives/${solutionId}/answers/${answerId}`,
      headers: HEADERS,
      crossDomain: true,
      data: data
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)

      doRedirect(error.status)
      reject(error)
    })
  })
}

const saveAnswer = (solutionId, data) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${process.env.SERVER}initiatives/${solutionId}/answers`,
      headers: HEADERS,
      crossDomain: true,
      dataType: 'json',
      data: data
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)

      doRedirect(error.status)
      reject(error)
    })
  })
}

const getAnswers = solutionId => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `${process.env.SERVER}initiatives/${solutionId}/answers`,
      headers: HEADERS,
      crossDomain: true,
      dataType: 'json'
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      doRedirect(error.status)
      reject(error)
    })
  })
}

const getLandAnswers = (id, limit) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `${process.env.SERVER}initiatives/${id}/answers?limit=${limit}`,
      headers: HEADERS,
      crossDomain: true,
      dataType: 'json'
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      reject(error)
    })
  })
}



export {
  removeAnswer,
  editAnswer,
  saveAnswer,
  getAnswers,
  getLandAnswers
}
