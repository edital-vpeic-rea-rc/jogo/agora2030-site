import axios from 'axios'
import { getCookie } from './helpers'

const EP = {
  find: (query) => `${process.env.SERVER}tags?text=${query}`,
  save: (initiativeId) => `${process.env.SERVER}initiatives/${initiativeId}/tags`,
  delete: (initiativeId, tagId) => `${process.env.SERVER}initiatives/${initiativeId}/tags/${tagId}`
}

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

function findTags (query) {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: EP.find(query),
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

function saveTag (initiativeId, data) {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: EP.save(initiativeId),
      data: data,
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

function removeTag (initiativeId, id) {
  return new Promise((resolve, reject) => {
    axios({
      method: 'delete',
      url: EP.delete(initiativeId, id),
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

export { findTags, saveTag, removeTag }
