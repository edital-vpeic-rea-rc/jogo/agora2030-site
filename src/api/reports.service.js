import { getCookie } from './helpers'

function getReport (reportType) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest()
    xhr.open('GET', `${process.env.SERVER}reports/${reportType}`, true)
    xhr.responseType = 'blob'
    xhr.setRequestHeader('Authorization', `Bearer ${getCookie('agoraToken')}`)
    xhr.onload = function (e) {
      if (this.status === 200) {
        let blob = new Blob([this.response], {type: 'application/vnd.ms-excel'})
        let downloadUrl = URL.createObjectURL(blob)
        resolve(downloadUrl)
      }
      if (this.status === 403 || this.status === 401) {
        window.location.href = '/403'
      } else {
        reject(this.response)
      }
    }
    xhr.send()
  })
}

export { getReport }
