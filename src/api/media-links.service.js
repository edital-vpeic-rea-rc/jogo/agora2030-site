import jquery from 'jquery'
import { getCookie } from './helpers'

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

    // getMedias: (id, type) => {
    //   let _type = (type != null) ? `/${type}` : ''
    //   `${process.env.SERVER}initiatives/${id}/medias/${_type}`
    // }

function saveMediaLink (link, initiativeId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/urls`,
      type: 'POST',
      data: link,
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro ao salvar link de media', status, xhr)
        if (xhr.status === 403) {
          window.location.href = '/403';
        }
        if (xhr.status === 401) {
          window.location.href = '/login';
        }
        reject(xhr)
      }
    })
  })
}

function deleteMediaLink (initiativeId, mediaLinkId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/urls/${mediaLinkId}`,
      type: 'DELETE',
      crossDomain: true,
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro ao apagar link de mídia', status, xhr)
        if (xhr.status === 403) {
          window.location.href = '/403';
        }
        if (xhr.status === 401) {
          window.location.href = '/login';
        }
        reject(xhr)
      }
    })
  })
}

function getMedias (type, initiativeId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/medias?type=${type}`,
      type: 'GET',
      crossDomain: true,
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro ao Obter links de mídia do tipo ' + type, status, xhr)
        if (xhr.status === 403) {
          window.location.href = '/403';
        }
        if (xhr.status === 401) {
          window.location.href = '/login';
        }
        reject(xhr)
      }
    })
  })
}

function getMediaLinks (type, initiativeId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/urls?type=${type}`,
      type: 'GET',
      crossDomain: true,
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro ao Obter links de mídia do tipo ' + type, status, xhr)
        if (xhr.status === 403) {
          window.location.href = '/403';
        }
        if (xhr.status === 401) {
          window.location.href = '/login';
        }
        reject(xhr)
      }
    })
  })
}

function getVimeoMeta (videoUrl) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `https://vimeo.com/api/oembed.json?url=${encodeURIComponent(videoUrl)}`,
      type: 'GET',
      crossDomain: true,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro ao Obter dados do vídeo do Vimeo ' + videoUrl, status, xhr)
        reject(xhr)
      }
    })
  })
}

function getSoundcloudMeta (audioUrl) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `https://soundcloud.com/oembed?url=${encodeURIComponent(audioUrl)}&format=json&maxheight=166&auto_play=true`,
      type: 'GET',
      crossDomain: true,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro ao Obter dados do vídeo do Vimeo ' + videoUrl, status, xhr)
        reject(xhr)
      }
    })
  })
}

export { saveMediaLink, getMediaLinks, getMedias, deleteMediaLink, getVimeoMeta, getSoundcloudMeta }
