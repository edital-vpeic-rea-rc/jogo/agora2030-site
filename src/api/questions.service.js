import jquery from 'jquery'
import { getCookie } from './helpers'

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

function getQuestionsAvailable (initiativeId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/questions/avaliables`,
      type: 'GET',
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log(status, xhr)
        reject(xhr)
        if (xhr.status === 403) {
          window.location.href = '/403'
        }
        if (xhr.status === 401) {
          window.location.href = '/login'
        }
      }
    })
  })
}

export { getQuestionsAvailable }
