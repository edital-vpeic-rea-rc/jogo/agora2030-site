import axios from 'axios'
import { getCookie } from './helpers'

const EP = {
  get: `${process.env.SERVER}curators`
}

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

const getCurators = () => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: EP.get,
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

export {
  getCurators
}
