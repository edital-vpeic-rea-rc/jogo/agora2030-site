import jquery from 'jquery'
import { getCookie } from './helpers'

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

function adminGetUsers (limit = 50, skip = 0) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}users?limit=${limit}&skip=${skip}&scope=resume`,
      type: 'GET',
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        reject(xhr)
        if (xhr.status === 403 || xhr.status === 401) {
          window.location.href = '/403'
        }
      }
    })
  })
}


export {
  adminGetUsers
}
