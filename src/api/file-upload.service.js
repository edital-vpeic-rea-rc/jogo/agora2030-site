import jquery from 'jquery'
import { getCookie } from './helpers'

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

function upload (formData, initiativeId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/medias`,
      type: 'POST',
      data: formData,
      async: false,
      cache: false,
      processData: false,
      contentType: false,
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro no Upload de arquivo', status, xhr)
        if (xhr.status === 403) {
          window.location.href = '/403'
        }
        if (xhr.status === 401) {
          window.location.href = '/login'
        }
        reject(xhr)
      }
    })
  })
}

function batchUpload (arrayData, initiativeId) {
  let arrayRequests = []

  arrayData.forEach(item => {
    arrayRequests.push(
      jquery.ajax({
        url: `${process.env.SERVER}initiatives/${initiativeId}/medias`,
        type: 'POST',
        data: item,
        async: false,
        cache: false,
        processData: false,
        contentType: false,
        headers: HEADERS,
        success: (response) => {
          // console.log(response)
          return response
        },
        error: (xhr, status) => {
          console.log('Erro no Upload de arquivo', status, xhr)
          if (xhr.status === 403) {
            window.location.href = '/403'
          }
          if (xhr.status === 401) {
            window.location.href = '/login'
          }
        }
      })
    )
  })

  return new Promise((resolve, reject) => {
    jquery.when.apply(null, arrayRequests).then(function () {
      let resp = []
      if (arrayRequests.length == 1) {
        resp.push(arguments[0])
      } else {
        for (let i = 0; i < arguments.length; i++) {
          resp.push(arguments[i][0])
        }
      }
      resolve(resp)
    }).done(() => {
      console.log('upload finalizado')
    })
  })
}

function getMedias (type, initiativeId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/medias?type=${type}`,
      type: 'GET',
      crossDomain: true,
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro ao Obter arquivos de mídia do tipo ' + type, status, xhr)
        if (xhr.status === 403) {
          window.location.href = '/403'
        }
        if (xhr.status === 401) {
          window.location.href = '/login'
        }
        reject(xhr)
      }
    })
  })
}

function editImageCaption (data, initiativeId, mediaId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/medias/${mediaId}`,
      type: 'PATCH',
      data,
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro ao editar Legenda', status, xhr)
        if (xhr.status === 403) {
          window.location.href = '/403'
        }
        if (xhr.status === 401) {
          window.location.href = '/login'
        }
        reject(xhr)
      }
    })
  })
}

function deleteMedia (initiativeId, mediaId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/medias/${mediaId}`,
      type: 'DELETE',
      crossDomain: true,
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro ao apagar arquivo', status, xhr)
        if (xhr.status === 403) {
          window.location.href = '/403'
        }
        if (xhr.status === 401) {
          window.location.href = '/login'
        }
        reject(xhr)
      }
    })
  })
}

function setCoverImage (imageId, initiativeId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/medias/${imageId}/setcover`,
      type: 'POST',
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        console.log('Erro ao configurar imagem de capa', status, xhr)
        if (xhr.status === 403) {
          window.location.href = '/403'
        }
        if (xhr.status === 401) {
          window.location.href = '/login'
        }
        reject(xhr)
      }
    })
  })
}

function getMedia (imageId, initiativeId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/medias/${imageId}`,
      type: 'GET',
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        if (xhr.status === 403) {
          window.location.href = '/403'
        }
        if (xhr.status === 401) {
          window.location.href = '/login'
        }
        reject(xhr)
      }
    })
  })
}

export { upload, batchUpload, editImageCaption, deleteMedia, getMedias, setCoverImage, getMedia, uploadAvatar }
